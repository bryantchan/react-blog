import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import * as actions from "../../store/actions/index";
import { makeStyles } from "@material-ui/core/styles";
import Spinner from "../../UI/spinner/spinner";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

const useStyles = makeStyles({
  post: {
    marginTop: "20px",
    "& a": {
      color: "#333",
      textDecoration: "none",
      "&:hover": {
        textDecoration: "underline"
      }
    }
  }
});

const Home = props => {
  const dispatch = useDispatch();
  const postsItems = useSelector(state => state.posts.items);
  const loading = useSelector(state => state.posts.loading);

  useEffect(() => {
    dispatch(actions.fetchPosts());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const classes = useStyles();

  let posts = postsItems.map((post, id) => (
    <Card className={classes.post} key={id}>
      <CardContent>
        <h3>
          <Link to={"/posts/" + post.id}>{post.title}</Link>
        </h3>
        <p>{post.body}</p>
      </CardContent>
    </Card>
  ));

  return (
    <Container className="home">{loading ? <Spinner /> : posts}</Container>
  );
};

export default Home;
