import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import * as actions from "../../store/actions/index";

import Container from "@material-ui/core/Container";
import Spinner from "../../UI/spinner/spinner";

const Post = props => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.posts.loading);
  const currentPost = useSelector(state => state.posts.current);

  useEffect(() => {
    dispatch(actions.fetchPostById(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const post = loading ? (
    <Spinner />
  ) : (
    <div className="post">
      <h1>{currentPost.title}</h1>
      <p>{currentPost.body}</p>
    </div>
  );

  return <Container>{post}</Container>;
};

export default Post;
