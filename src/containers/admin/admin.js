import React from "react";
import Container from "@material-ui/core/Container";
import { Route, useRouteMatch, Switch, Redirect } from "react-router-dom";

import PostList from "./posts/post-list";
import PostEdit from "./posts/post-edit";
import PostCreate from "./posts/post-create";

export default function Admin() {
  const { url } = useRouteMatch();
  return (
    <Container className="admin">
      <Switch>
        <Route path={url + "/posts/create"} exact component={PostCreate} />
        <Route path={url + "/posts/:id"} exact component={PostEdit} />
        <Route path={url + "/posts"} component={PostList} />
        <Redirect to={url + "/posts"} />
      </Switch>
    </Container>
  );
}
