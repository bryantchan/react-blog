import React from "react";
import firebase from "firebase/app";
import "firebase/database";
import { useHistory, useParams } from "react-router-dom";

import PostControl from "../../../components/post/control";

const PostEdit = props => {
  const history = useHistory();
  const { id } = useParams();

  const [post, setPost] = React.useState(null);

  React.useEffect(() => {
    firebase
      .database()
      .ref("/posts/" + id)
      .once("value")
      .then(snapshot => {
        setPost(snapshot.val());
      })
      .catch(err => {
        console.log(err);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const onSubmit = post => {
    firebase
      .database()
      .ref("/posts/" + id)
      .set(post)
      .then(() => {
        history.push("/admin/posts");
      })
      .catch(err => console.log(err));
  };

  return post ? <PostControl post={post} submit={onSubmit} /> : null;
};

export default PostEdit;
