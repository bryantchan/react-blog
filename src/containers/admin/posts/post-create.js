import React from "react";
import firebase from "firebase/app";
import "firebase/database";
import { useHistory } from "react-router-dom";

import PostControl from "../../../components/post/control";

const PostCreate = props => {
  const history = useHistory();
  console.log();
  const db = firebase.database();

  const onSubmit = post => {
    db.ref()
      .child("posts")
      .push(post)
      .then(res => {
        history.push("/admin/posts");
      });
  };

  return <PostControl post={{ title: "", body: "" }} submit={onSubmit} />;
};

export default PostCreate;
