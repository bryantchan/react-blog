import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import firebase from "firebase/app";
import "firebase/database";

import * as actions from "../../../store/actions/index";

import Button from "@material-ui/core/Button";
import Spinner from "../../../UI/spinner/spinner";
import Post from "../../../components/post/post";

const PostList = props => {
  const dispatch = useDispatch();
  const postsItems = useSelector(state => state.posts.items);
  const loading = useSelector(state => state.posts.loading);

  React.useEffect(() => {
    dispatch(actions.fetchPosts());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onDelete = id => {
    firebase
      .database()
      .ref("/posts/" + id)
      .remove()
      .then(() => {
        dispatch(actions.fetchPosts());
      })
      .catch(err => console.log(err));
  };
  const posts = loading ? (
    <Spinner />
  ) : (
    postsItems.map(post => {
      return <Post onDelete={onDelete} item={post} key={post.id} />;
    })
  );
  return (
    <React.Fragment>
      <Button variant="contained" color="secondary">
        <Link to="/admin/posts/create">Add Post</Link>
      </Button>
      <div className="posts">{posts}</div>
    </React.Fragment>
  );
};

export default PostList;
