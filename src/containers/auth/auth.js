import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, useParams } from "react-router-dom";

import AuthForm from "../../components/authForm/form";

import * as actions from "../../store/actions/index";

const Login = props => {
  const { method } = useParams();
  const dispatch = useDispatch();
  const isAuth = useSelector(state => state.auth.isAuth);

  const handleSubmit = (email, password) => {
    const payload = { email, password, method };
    dispatch(actions.startAuth(payload));
  };

  React.useEffect(() => {
    if (method === "logout") dispatch(actions.startLogout());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return isAuth ? (
    <Redirect to="/home" />
  ) : (
    <AuthForm method={method} auth={handleSubmit} />
  );
};

export default Login;
