import * as actionTypes from "../actions/actionTypes";

export function checkAuth() {
  return { type: actionTypes.CHECK_AUTH };
}

export function startAuth(payload) {
  return { type: actionTypes.START_AUTH, payload };
}

export function loginSuccess(user) {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    user
  };
}

export function startLogout() {
  return { type: actionTypes.START_LOGOUT };
}
export function setLogout() {
  return { type: actionTypes.LOGOUT };
}
