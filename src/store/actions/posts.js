import * as actionTypes from "./actionTypes";

export const fetchPostsStart = () => {
  return {
    type: actionTypes.FETCH_POSTS_START
  };
};

export function fetchPosts() {
  return { type: actionTypes.FETCH_POSTS };
}

export const fetchPostsSuccess = posts => {
  return {
    type: actionTypes.FETCH_POSTS_SUCCESS,
    posts
  };
};

export const fetchPostsFailed = () => {
  return {
    type: actionTypes.FETCH_POSTS_FAILED
  };
};

export function fetchPostById(id) {
  return { type: actionTypes.FETCH_POST_BYID, id };
}

export const fetchPostSuccess = post => {
  return {
    type: actionTypes.FETCH_POST_BYID_SUCCESS,
    post
  };
};
