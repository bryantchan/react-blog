import { call, takeEvery, put } from "redux-saga/effects";
import * as actionsTypes from "../actions/actionTypes";
import * as actions from "../actions/index";
import { sagaFirebase } from "../firebase";

function* fetchPosts() {
  yield put(actions.fetchPostsStart());
  const postsObj = yield call(sagaFirebase.database.read, "posts");
  const posts = [];
  yield Object.keys(postsObj).forEach(id => {
  if (postsObj) {
      posts.push({
        ...postsObj[id],
        id
      });
    });
  }
  yield put(actions.fetchPostsSuccess(posts));
}

function* fetchPostById({ id }) {
  try {
    const post = yield call(sagaFirebase.database.read, `posts/${id}`);
    yield put(actions.fetchPostSuccess(post));
  } catch (e) {
    yield put(actions.fetchPostsFailed());
  }
}

export default function* postsSaga() {
  yield takeEvery(actionsTypes.FETCH_POSTS, fetchPosts);
  yield takeEvery(actionsTypes.FETCH_POST_BYID, fetchPostById);
}
