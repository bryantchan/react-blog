import { call, put, takeEvery } from "redux-saga/effects";
import firebase, { sagaFirebase } from "../firebase";
import "firebase/auth";

import * as actionTypes from "../actions/actionTypes";
import * as actions from "../actions";

const wathAuth = () => {
  return new Promise((resolve, reject) => {
    firebase.auth().onAuthStateChanged(user => {
      resolve(user);
    });
  });
};

function* checkAuth(action) {
  const currentUser = yield call(wathAuth);
  if (currentUser) {
    yield put(actions.loginSuccess(currentUser));
  } else {
    yield put(actions.setLogout());
  }
}

function* auth({ payload: { email, password, method } }) {
  const formData = [email, password];
  const authRef =
    method === "login"
      ? sagaFirebase.auth.signInWithEmailAndPassword
      : sagaFirebase.auth.createUserWithEmailAndPassword;
  try {
    yield call(authRef, ...formData);
    yield put(actions.loginSuccess(firebase.auth().currentUser));
  } catch (e) {}
}

function* logout() {
  try {
    yield firebase.auth().signOut();
    yield put(actions.setLogout());
  } catch (e) {}
}

function* authSaga() {
  yield takeEvery(actionTypes.CHECK_AUTH, checkAuth);
  yield takeEvery(actionTypes.START_AUTH, auth);
  yield takeEvery(actionTypes.START_LOGOUT, logout);
}

export default authSaga;
