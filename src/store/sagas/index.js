import { call, all } from "redux-saga/effects";

import authSaga from "./auth";
import postsSaga from "./posts";

export default function* mySagas() {
  yield all([call(authSaga), call(postsSaga)]);
}
