import firebase from "firebase/app";
import ReduxSagaFirebase from "redux-saga-firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBp9KqAL0J_4Lnc3fCZe_5rUgrswStXx84",
  authDomain: "react-blog-3c5d8.firebaseapp.com",
  databaseURL: "https://react-blog-3c5d8.firebaseio.com",
  projectId: "react-blog-3c5d8",
  storageBucket: "react-blog-3c5d8.appspot.com",
  messagingSenderId: "146858600766",
  appId: "1:146858600766:web:e6446b695b74dde5d9e46f"
};

const fb = firebase.initializeApp(firebaseConfig);

export const sagaFirebase = new ReduxSagaFirebase(fb);
export default fb;
