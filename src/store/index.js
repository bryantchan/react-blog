import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import createSagaMiddleware from "redux-saga";

import postsReducer from "./reducers/posts";
import authReducer from "./reducers/auth";
import mySagas from "./sagas/index";

const rootReducer = combineReducers({
  posts: postsReducer,
  auth: authReducer
});

const sagaMiddleware = createSagaMiddleware();

const middlewares = [thunk, sagaMiddleware];

const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...middlewares))
);

sagaMiddleware.run(mySagas);

export default store;
