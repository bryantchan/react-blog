import * as actionTypes from "../actions/actionTypes";

const initialState = {
  loading: false,
  items: [],
  current: { title: "", body: "" }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_POSTS_START:
      return {
        ...state,
        loading: true
      };
    case actionTypes.FETCH_POSTS_FAILED:
      return {
        ...state,
        loading: false
      };
    case actionTypes.FETCH_POSTS_SUCCESS:
      return {
        ...state,
        items: action.posts,
        loading: false
      };
    case actionTypes.FETCH_POST_BYID_SUCCESS:
      return {
        ...state,
        current: action.post,
        loading: false
      };
    default:
      return state;
  }
};

export default reducer;
