import * as actionTypes from "../actions/actionTypes";

const initialState = {
  isAuth: false,
  user: null,
  loading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHECK_AUTH:
      return {
        ...state,
        isAuth: action.isAuth,
        user: action.user
      };
    case actionTypes.AUTH_START:
      return {
        ...state,
        loading: true
      };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        isAuth: true,
        user: action.user,
        loading: false
      };
    case actionTypes.LOGOUT:
      return {
        ...state,
        isAuth: false,
        user: null,
        loading: false
      };
    default:
      return state;
  }
};

export default reducer;
