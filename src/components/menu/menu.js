import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1
  },
  menu: {
    "& a": {
      color: "white",
      textDecoration: "none"
    }
  }
}));

const Menu = props => {
  const classes = useStyles();
  let links = null;

  if (props.isAuth) {
    links = (
      <React.Fragment>
        <Button>
          <Link to="/admin/posts">admin</Link>
        </Button>
        <Button>
          <Link to="/auth/logout">logout</Link>
        </Button>
      </React.Fragment>
    );
  } else {
    links = (
      <Button>
        <Link to="/auth/login">login</Link>
      </Button>
    );
  }

  return (
    <div className={classes.menu}>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            React Blog
          </Typography>
          <Button>
            <Link to="/home">Home</Link>
          </Button>
          {links}
        </Toolbar>
      </AppBar>
    </div>
  );
};

const matStateToProps = state => {
  return {
    isAuth: state.auth.isAuth
  };
};

export default connect(matStateToProps)(Menu);
