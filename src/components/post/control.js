import React from "react";
import {
  TextareaAutosize,
  FormControl,
  InputLabel,
  Input,
  Button,
  makeStyles
} from "@material-ui/core";
import useForm from "react-hook-form";

const useStyles = makeStyles({
  textarea: {
    width: "100%",
    margin: "20px 0 20px",
    border: "1px solid #eee",
    fontSize: "15px",
    lineHeight: "1.5",
    padding: "5px",
    "&:focus": {
      border: "1px solid #efefef",
      outline: "none"
    }
  }
});

const PostControl = props => {
  const classes = useStyles();
  const { register, handleSubmit, errors } = useForm();
  const [post, setPost] = React.useState({
    ...props.post
  });
  console.log(errors);

  const changeInput = e => {
    const newPost = Object.assign({}, post);
    newPost[e.target.name] = e.target.value;
    setPost(newPost);
  };

  return (
    <div className="post-create">
      <form action="" onSubmit={handleSubmit(props.submit)}>
        <FormControl fullWidth>
          <InputLabel htmlFor="post-title">Post Title</InputLabel>
          <Input
            value={post.title}
            id="post-title"
            name="title"
            onChange={changeInput}
            inputRef={register({ required: true })}
          />
        </FormControl>
        <TextareaAutosize
          id="post-content"
          rows="20"
          value={post.body}
          onChange={changeInput}
          className={classes.textarea}
          placeholder="Post Content"
          name="body"
          ref={register({ required: true })}
        />
        <Button type="submit" variant="contained" color="secondary">
          Save
        </Button>
      </form>
    </div>
  );
};

export default PostControl;
