import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

// UI
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { red } from "@material-ui/core/colors";

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 345
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  post: {
    marginTop: "20px",
    "& a": {
      color: "#333",
      textDecoration: "none",
      "&:hover": {
        textDecoration: "underline"
      }
    }
  }
}));

const Post = props => {
  const post = props.item;
  const classes = useStyles();

  return (
    <Card className={classes.post}>
      <CardContent>
        <h3>
          <Link to={"/posts/" + post.id}>{post.title}</Link>
        </h3>
        <p>{post.body}</p>
      </CardContent>
      <CardActions disableSpacing>
        <Link to={"/admin/posts/" + post.id}>
          <IconButton aria-label="add to favorites">
            <EditIcon />
          </IconButton>
        </Link>
        <IconButton
          aria-label="share"
          onClick={() => {
            props.onDelete(post.id);
          }}
        >
          <DeleteIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default Post;
