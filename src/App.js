import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { useDispatch } from "react-redux";

import Menu from "./components/menu/menu";
import Home from "./containers/home/home";
import Post from "./containers/home/post";
import Admin from "./containers/admin/admin";
import Auth from "./containers/auth/auth";
import Box from "@material-ui/core/Box";

import * as actions from "./store/actions/index";

const App = props => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(actions.checkAuth());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box className="app">
      <Menu />
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/posts/:id" component={Post} />
        <Route path="/admin" component={Admin} />
        <Route path="/auth/:method" component={Auth} />
        <Redirect to="/home" />
      </Switch>
    </Box>
  );
};

export default App;
